﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MathC {
	// Random
	public static float getExponantialVariate(float lambda, float lowerBound, float upperBound) {
		float rand = Mathf.Clamp(Random.value, lowerBound, upperBound);

		return - Mathf.Log(rand) / lambda;
	}

	// Camera size
	public static Vector2 calculateCameraSize(Camera camera) {
		float height = 2f * camera.orthographicSize;
		float width = height * camera.aspect;

		return new Vector2(width, height);
	}

	// Vector math
	public static Vector2 left (Vector2 v) {
		return new Vector2(-v.y,  v.x);
	}

	public static Vector2 right(Vector2 v) {
		return new Vector2( v.y, -v.x);
	}

	public static float cross(Vector2 A, Vector2 B) {
		return A.x * B.y - A.y * B.x;
	}

	// Line segments
	public static void lineIntersection(Vector2 A, Vector2 B, Vector2 C, Vector2 D, out float t, out float u, out float denominator) {
		Vector2 AB = B - A;
		Vector2 CD = D - C;

		Vector2 AC = C - A;

		denominator = cross(AB, CD);

		t = cross(AC, CD) / denominator;
		u = cross(AC, AB) / denominator;
	}

	public static int getWinding(List<Vector2> points) {
		int oldSign = 0;

		for (int n = 0; n < points.Count - 2; n++) {
			Vector2 A = points[n    ];
			Vector2 B = points[n + 1];
			Vector2 C = points[n + 2];

			int sign = (int) Mathf.Sign(MathC.cross(B - A, C - B));

			if (oldSign != 0 && sign != oldSign) {
				return 0;
			}

			oldSign = sign;
		}

		return oldSign;
	}

	public static float difficultyMod(float difficulty, float min, float max) {
		return min + difficulty * (max - min);
	}
}
