﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	protected Rigidbody2D target = null;

	// Use this for initialization
	void Awake () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		fixedUpdate();
	}

	protected void fixedUpdate() {
		if (!target)
			return;

		Camera camera = Camera.main;
		Vector2 cameraSize = MathC.calculateCameraSize(camera);

		Vector3 position = target.position;

		position.z = -10;
		transform.position = position;
	}
}
