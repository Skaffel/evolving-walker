﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimalData {
	public int muscleLength = 1;

	public Vector2[] nodes;
	public bool[] isHead;
	
	public bool[,] isConnected;
	public float[,,] muscles;
	public float[,,] longitudnalMuscles;

	public AnimalData(GameObject[] nodes, int muscleLength) {
		this.muscleLength = muscleLength;

		this.nodes   = new Vector2[nodes.Length];
		this.isHead = new bool[nodes.Length];

		this.isConnected = new bool[nodes.Length, nodes.Length];
		
		this.muscles            = new float[nodes.Length, nodes.Length, muscleLength];
		this.longitudnalMuscles = new float[nodes.Length, nodes.Length, muscleLength];
		
		for (int x = 0; x < nodes.Length; x++) {
			this.nodes[x] = nodes[x].transform.position;

			MuscleHandler handler = nodes[x].GetComponent<MuscleHandler>();

			if (!handler) {
				continue;
			}

			if (handler.isHead) {
				isHead[x] = true;
			}

			for (int y = 0; y < handler.targets.Count; y++) {
				int index = System.Array.IndexOf(nodes, handler.targets[y].gameObject);

				isConnected[x, index] = true;
				for (int z = 0; z < muscleLength; z++) {
					muscles           [x, index, z] = handler.           muscles[y].Throttle;
					longitudnalMuscles[x, index, z] = handler.longitudnalMuscles[y].Throttle;
				}
			}
		}
	}

	private AnimalData(AnimalData animalData) {
		this.muscleLength = animalData.muscleLength;

		this.isHead      = animalData.isHead.Clone() as bool[];
		this.nodes       = animalData.nodes.Clone() as Vector2[];

		this.isConnected = animalData.isConnected.Clone() as bool[,];
		
		this.muscles            = animalData.           muscles.Clone() as float[,,];
		this.longitudnalMuscles = animalData.longitudnalMuscles.Clone() as float[,,];
	}

	public AnimalData clone() {
		return new AnimalData(this);
	}

	public void mutate(float mutationProbability, float mutationMagnitude) {
		for (int x = 0; x < nodes.Length; x++) {
			if (Random.value < mutationProbability) {
				nodes[x].x += Random.Range(-mutationMagnitude, mutationMagnitude) * 0.1f;
			}
			if (Random.value < mutationProbability) {
				nodes[x].y += Random.Range(-mutationMagnitude, mutationMagnitude) * 0.1f;
			}
		}

		for (int x = 0; x < nodes.Length; x++) {
			for (int y = 0; y < nodes.Length; y++) {
				if (isConnected[x, y]) {
				 	for (int z = 0; z < muscleLength; z++) {
				 		if (Random.value < mutationProbability) {
							muscles[x, y, z] += Random.Range(-mutationMagnitude, mutationMagnitude);

							muscles[x, y, z] = Mathf.Clamp(muscles[x, y, z], -1f, 1f);
						}
						if (Random.value < mutationProbability) {
							longitudnalMuscles[x, y, z] += Random.Range(-mutationMagnitude, mutationMagnitude);

							longitudnalMuscles[x, y, z] = Mathf.Clamp(muscles[x, y, z], -1f, 1f);
						}
					}
				}
			}
		}
	}

	public void crossOver(AnimalData other) {
		int minX = Random.Range(0, nodes.Length);
		int maxX = minX + Random.Range(0, nodes.Length);

		int minY = Random.Range(0, nodes.Length);
		int maxY = minY + Random.Range(0, nodes.Length);

		int minZ = Random.Range(0, muscleLength);
		int maxZ = minZ + Random.Range(0, muscleLength);

		for (int x = minX; x <= maxX; x++) {
			int xx = x % nodes.Length;

			nodes[xx] = other.nodes[xx];
		}

		for (int x = minX; x <= maxX; x++) {
			for (int y = minY; y <= maxY; y++) {
				int xx = x % nodes.Length;
				int yy = y % nodes.Length;

				if (isConnected[xx, yy]) {
					for (int z = minZ; z < maxZ; z++) {
						int zz = z % muscleLength;

						           muscles[xx, yy, zz] = other.           muscles[xx, yy, zz];
						longitudnalMuscles[xx, yy, zz] = other.longitudnalMuscles[xx, yy, zz];
					}
				}
			}
		}
	}

	public GameObject[] createAnimal(GameObject Node, Vector2 offset) {
		List<GameObject> nodes = new List<GameObject>();

		for (int x = 0; x < this.nodes.Length; x++) {
			GameObject node = GameObject.Instantiate(Node, this.nodes[x] + offset, Quaternion.identity) as GameObject;

			nodes.Add(node);

			node.GetComponent<Rigidbody2D>().isKinematic = true;

			if (isHead[x]) {
				node.GetComponent<MuscleHandler>().setHead();
			}
		}

		for (int x = 0; x < nodes.Count; x++) {
			for (int y = 0; y < nodes.Count; y++) {
				if (isConnected[x, y]) {
					NodeFactory.connectNode(nodes[x].GetComponent<Rigidbody2D>(), nodes[y].GetComponent<Rigidbody2D>(), extractMuscle(x, y), extractLongitudnalMuscle(x, y));
				}
			}
		}

		return nodes.ToArray();
	}

	private float[] extractMuscle(int x, int y) {
		float[] singleMuscle = new float[muscleLength];

		for (int z = 0; z < muscleLength; z++) {
			singleMuscle[z] = muscles[x, y, z];
		}

		return singleMuscle;
	}

	private float[] extractLongitudnalMuscle(int x, int y) {
		float[] singleMuscle = new float[muscleLength];

		for (int z = 0; z < muscleLength; z++) {
			singleMuscle[z] = longitudnalMuscles[x, y, z];
		}

		return singleMuscle;
	}
}
