﻿using UnityEngine;
using System.Collections;

public static class NodeFactory {
	public static void connectNode(Rigidbody2D origin, Rigidbody2D target, float[] throttle, float[] longitudnalThrottle) {
		Vector2 AB = (target.position - origin.position).normalized;

		SpringJoint2D joint = origin.gameObject.AddComponent<SpringJoint2D>() as SpringJoint2D;

		joint.connectedBody = target;
		joint.distance = (target.position - origin.position).magnitude;
		joint.frequency = 1.2f;

		//
		Muscle muscle = origin.gameObject.AddComponent<Muscle>() as Muscle;

		muscle.target = target;
		muscle.ThrottleArray = throttle;

		//
		LongitudnalMuscle longitudnalMuscle = origin.gameObject.AddComponent<LongitudnalMuscle>() as LongitudnalMuscle;

		longitudnalMuscle.target = target;
		longitudnalMuscle.ThrottleArray = longitudnalThrottle;

		//
		origin.GetComponent<MuscleHandler>().addMuscle(target, muscle, longitudnalMuscle, joint, AB);
	}
}
