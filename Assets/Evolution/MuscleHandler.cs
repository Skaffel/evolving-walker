﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MuscleHandler : MonoBehaviour {
	public  GameObject Line = null;
	public  GameObject Eye = null;

	public float angle = 0;

	public bool isHead = false;

	public List<Rigidbody2D> targets = new List<Rigidbody2D>();
	public List<Muscle> muscles = new List<Muscle>();
	public List<LongitudnalMuscle> longitudnalMuscles = new List<LongitudnalMuscle>();
	public List<Joint2D> joints = new List<Joint2D>();
	
	public List<float> angles          = new List<float>();
	
	public List<float> connectedAngles = new List<float>();
	public List<Rigidbody2D> connectedBody = new List<Rigidbody2D>();

	public List<float> modifiedAngles = new List<float>();
	public List<float> targetModifiedAngles = new List<float>();

	public List<Vector2> modifiedDirections       = new List<Vector2>();
	public List<Vector2> targetModifiedDirections = new List<Vector2>();

	public List<MuscleHandler> targetHandlers = new List<MuscleHandler>();
	public List<float> targetAngles = new List<float>();

	void Start() {

	}

	void FixedUpdate() {
		Rigidbody2D rigidBody = GetComponent<Rigidbody2D>();

		float[] diffAngles = new float[angles.Count + connectedAngles.Count];

		for (int n = 0; n < angles.Count; n++) {
			Vector2 dir = targets[n].position - rigidBody.position;

			float currentAngle = Mathf.Atan2(dir.y, dir.x);
			float targetAngle  = CircularMath.addition(angle, angles[n]);

			diffAngles[n] = CircularMath.subtraction(currentAngle, targetAngle);

			modifiedAngles[n] = diffAngles[n];
			modifiedDirections[n] = new Vector2(Mathf.Cos(targetAngle), Mathf.Sin(targetAngle));

			currentAngle = Mathf.Atan2(-dir.y, -dir.x);
			targetAngle  = CircularMath.addition(targetHandlers[n].angle, targetAngles[n]);

			targetModifiedAngles[n] = CircularMath.subtraction(currentAngle, targetAngle);
			targetModifiedDirections[n] = new Vector2(Mathf.Cos(targetAngle), Mathf.Sin(targetAngle));
		}
		for (int n = 0; n < connectedAngles.Count; n++) {
			Vector2 dir = connectedBody[n].position - rigidBody.position;

			float currentAngle = Mathf.Atan2(dir.y, dir.x);
			float targetAngle  = CircularMath.addition(angle, connectedAngles[n]);

			diffAngles[angles.Count + n] = CircularMath.subtraction(currentAngle, targetAngle);
		}

		angle = CircularMath.subtraction(CircularMath.mean(diffAngles), angle);

		//transform.eulerAngles = new Vector3(0, 0, angle * 180 / Mathf.PI);
	}

	public void addMuscle(Rigidbody2D target, Muscle muscle, LongitudnalMuscle longitudnalMuscle, Joint2D joint, Vector2 direction) {
		targets.Add(target);
		muscles.Add(muscle);
		longitudnalMuscles.Add(longitudnalMuscle);
		joints .Add(joint);

		AngleSpring angleSpring = gameObject.AddComponent<AngleSpring>() as AngleSpring; 
		angleSpring.handler = this;
		angleSpring.index = angles.Count;
		angleSpring.target = target;


		GameObject line = Instantiate(Line, transform.position, Quaternion.identity) as GameObject;

		line.transform.parent = transform;
		line.transform.position = line.transform.position - new Vector3(0, 0, 5);

		Arm arm = line.GetComponent<Arm>();

		arm.origin = GetComponent<Rigidbody2D>();
		arm.target = target;

		arm.handler = this;

		arm.index = angles.Count;

		arm.joint = joint as SpringJoint2D;

		float angle          = Mathf.Atan2( direction.y,  direction.x);
		float connectedAngle = Mathf.Atan2(-direction.y, -direction.x);

		angles.Add(angle);
		target.GetComponent<MuscleHandler>().connectedAngles.Add(connectedAngle);
		target.GetComponent<MuscleHandler>().connectedBody.Add(GetComponent<Rigidbody2D>());

		modifiedDirections.Add(new Vector2());
		targetModifiedDirections.Add(new Vector2(0, 0));

		targetHandlers.Add(target.GetComponent<MuscleHandler>());
		targetAngles.Add(connectedAngle);

		modifiedAngles.Add(0);
		targetModifiedAngles.Add(0);
	}

	public void setHead() {
		gameObject.AddComponent<DetectDeath>();

		GameObject eye = Instantiate(Eye, transform.position, Quaternion.identity) as GameObject;

		eye.transform.parent = transform;
		eye.transform.position = (Vector2) (eye.transform.position) + new Vector2(0.1f, 0.2f);

		isHead = true;
	}
}
