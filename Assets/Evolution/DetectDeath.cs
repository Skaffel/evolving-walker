﻿using UnityEngine;
using System.Collections;

public class DetectDeath : MonoBehaviour {


	// Update is called once per frame
	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.tag == "Ground") {
			GameObject.Find("Input").BroadcastMessage("animalDied", SendMessageOptions.DontRequireReceiver);
		}
	}
}
