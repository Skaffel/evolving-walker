﻿using UnityEngine;
using FullInspector;
using System.Collections;
using System.Collections.Generic;

public class Creator : BaseBehavior {
	public Dictionary<State, MonoBehaviour> states = new Dictionary<State, MonoBehaviour>();
	
	protected State state = State.None;
	
	void Start() {
		changeState();
	}

	void Update() {
		State oldState = state;

		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			state = State.Drag;
		}
		if (Input.GetKeyDown(KeyCode.Alpha2)) {
			state = State.Create;
		}
		if (Input.GetKeyDown(KeyCode.Alpha3)) {
			state = State.Connect;
		}


		if (oldState != state) {
			changeState();
		}
	}


	protected void changeState() {
		foreach (MonoBehaviour b in states.Values) {
			b.enabled = false;
		}

		if (states.ContainsKey(state)) {
			states[state].enabled = true;
		}
	}

	public enum State {
		None,
		Drag, 
		Create,
		Connect,
	}
}
