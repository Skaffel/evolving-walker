﻿using UnityEngine;
using System.Collections.Generic;

public class EvolutionManager : MonoBehaviour {
	public GameObject Node = null;

	public int currentAnimal = 0;
	public List<AnimalData> animalData = new List<AnimalData>();
	public List<float> score = new List<float>();

	public Simulation simulation = null;

	public int generation = 0;
	public int numberOfAnimals = 20;
	public AnimalData baseAnimal = null;

	protected bool currentFinished = false;
	protected bool startNext = false;

	void OnEnable() {
		generation = 0;
		simulation.enabled = false;

		if (baseAnimal == null)
			return;

		generateAnimalsFromBase();
	}

	void OnDisable() {
		currentFinished = false;
		startNext = false;
	}

	void Update() {
		if (currentFinished && currentAnimal < animalData.Count) {
			score[currentAnimal] = simulation.score;

			currentAnimal++;

			currentFinished = false;
			startNext = true;

			clean();

			simulation.enabled = false;
			simulation.disable();
		}
		if (startNext && currentAnimal < animalData.Count) {
			GameObject[] animal = animalData[currentAnimal].createAnimal(Node, new Vector2());

			simulation.enabled = true;
			simulation.newSimulation(animal);
		}

		if (startNext) {
			startNext = false;
		}

		if (currentAnimal >= animalData.Count) {
			generation++;

			int[] indices = new int[animalData.Count];

			for (int n = 0; n < animalData.Count; n++) {
				indices[n] = n;
			} 

   			System.Array.Sort(score.ToArray(), indices);
   			System.Array.Reverse(indices);

   			int save = (int) (animalData.Count * 0.3f);

   			List<AnimalData> newAnimalData = new List<AnimalData>();
   			List<float> 	 newScore 	   = new List<float>();

   			currentAnimal = 0;
   			for (int n = 0; n < save; n++) {
   				newAnimalData.Add(animalData[indices[n]]);
   				newScore.Add(score[indices[n]]);

   				currentAnimal = n + 1;
   			}

   			animalData = newAnimalData;
   			score      = newScore;
   			
   			while (animalData.Count < numberOfAnimals) {
   				int index = Random.Range(0, save);
   				int crossOver = Random.Range(index + 1, save - 1) % save;

   				AnimalData newAnimal = animalData[index].clone();
   				
   				newAnimal.crossOver(animalData[crossOver]);
   				newAnimal.mutate(0.1f, 0.2f);

   				animalData.Add(newAnimal);
   				score.Add(0);
   			}

   			startNext = true;
		}
	}

	public void animalDied() {
		currentFinished = true;
	}

	protected void generateAnimalsFromBase() {
		animalData = new List<AnimalData>();
		score = new List<float>();

		for (int n = 0; n < numberOfAnimals; n++) {
			AnimalData newAnimal = baseAnimal.clone();

			newAnimal.mutate(1f, 2f); 

			animalData.Add(newAnimal);
			score.Add(0);
		}

		currentAnimal = 0;
		startNext = true;
	}

	protected void clean() {
		GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Node");

		for (int n = 0; n < gameObjects.Length; n++) {
			gameObjects[n].tag = "Untagged";
			Destroy(gameObjects[n]);
		}
	}
}
