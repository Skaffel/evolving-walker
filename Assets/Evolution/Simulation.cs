﻿using UnityEngine;
using System.Collections;

public class Simulation : MonoBehaviour {
	public float score = 0;
	public GameObject camera = null;

	public float maxDistance = 40;

	protected float startDistance = 0;

	protected GameObject[] animal;
	protected float stopTime = 0;

	protected float startTime = 0;

	public void newSimulation(GameObject[] animal) {
		this.animal = animal;

		for (int n = 0; n < animal.Length; n++) {
			animal[n].GetComponent<Rigidbody2D>().isKinematic = false;
		}

		startDistance = calculateDistance();
		score = 0;

		stopTime = Time.time + 5;
		startTime = Time.time;
	}

	public void disable() {
		for (int n = 0; animal != null && n < animal.Length; n++) {
			Destroy(animal[n]);
		}

		animal = null;

		if (camera) {
			camera.transform.position = new Vector2(0, 1);
			camera.transform.position = new Vector3(camera.transform.position.x, camera.transform.position.y, -10);
		}
	}

	void OnDisable() {
		disable();
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (animal.Length == 0 || !animal[0])
			return;

		float currentScore = calculateDistance() - startDistance;

		if (currentScore > score) {
			score = currentScore;
			stopTime = Time.time + 5;
		}

		if (score >= maxDistance) {
			score = maxDistance + 100 / (Time.time - startTime);

			transform.root.BroadcastMessage("animalDied", SendMessageOptions.DontRequireReceiver);
		}

		camera.transform.position = calculatePosition();
		camera.transform.position = new Vector3(camera.transform.position.x, camera.transform.position.y, -10);

		if (Time.time > stopTime) {
			transform.root.BroadcastMessage("animalDied", SendMessageOptions.DontRequireReceiver);
		}
	}

	protected Vector2 calculatePosition() {
		Vector2 position = new Vector2();

		foreach (GameObject obj in animal) {
			position += (Vector2) obj.transform.position;
		}

		position /= animal.Length;

		return position;
	}

	protected float calculateDistance() {
		return calculatePosition().x;
	}
}
