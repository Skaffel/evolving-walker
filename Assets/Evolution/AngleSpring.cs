﻿using UnityEngine;
using System.Collections;

public class AngleSpring : MonoBehaviour {
	public float strength = 600;
	public float damping  = 0;

	public MuscleHandler handler = null;
	public int index;

	public Rigidbody2D target;

	protected Rigidbody2D _origin;
	public Rigidbody2D origin {
		get {return _origin;}
	}

	void Start() {
		_origin = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate() {
		if (target && origin) {
			Vector2 dir = (target.position - origin.position).normalized;

			float throttle = -(handler.targetModifiedAngles[index] + handler.modifiedAngles[index]) / 2f; 

			origin.AddForce(MathC.left (dir) * throttle * (strength - 1 * damping));
			target.AddForce(MathC.right(dir) * throttle * (strength - 1 * damping));
		}
	}
}
