﻿using UnityEngine;
using System.Collections;

public class LongitudnalMuscle : MonoBehaviour {
	public float strength = 200;

	public Rigidbody2D target;

	protected Rigidbody2D _origin;
	public Rigidbody2D origin {
		get {return _origin;}
	}

	protected float[] throttle = null;
	protected float currentThrottle = 0;
	public float Throttle {
		get {
			if (throttle != null) {
				return throttle[(int) currentThrottle];
			}
			return 0;
		}
	}

	public float[] ThrottleArray {
		get {return throttle;}
		set {
			throttle = value;

			for (int n = 0; n < throttle.Length; n++) {
				throttle[n] = Mathf.Clamp(throttle[n], -1f, 1f);
			}
		}
	}

	void Start() {
		_origin = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate() {
		if (target && origin && throttle != null) {
			int currentThrottleIndex = (int) currentThrottle;

			Vector2 dir = (target.position - origin.position).normalized;

			origin.AddForce(-dir * throttle[currentThrottleIndex] * strength);
			target.AddForce( dir * throttle[currentThrottleIndex] * strength);

			currentThrottle = (currentThrottle + Time.fixedDeltaTime * 3) % throttle.Length;
		}
	}
}
