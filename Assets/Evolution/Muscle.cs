﻿using UnityEngine;
using System.Collections;

public class Muscle : MonoBehaviour {
	public float strength = 200;
	public float damping  = 0;

	public Rigidbody2D target;

	protected Rigidbody2D _origin;
	public Rigidbody2D origin {
		get {return _origin;}
	}

	protected float[] throttle = null;
	protected float currentThrottle = 0;
	public float Throttle {
		get {
			if (throttle != null) {
				return throttle[(int) currentThrottle];
			}
			return 0;
		}
	}

	public float[] ThrottleArray {
		get {return throttle;}
		set {
			throttle = value;

			for (int n = 0; n < throttle.Length; n++) {
				throttle[n] = Mathf.Clamp(throttle[n], -1f, 1f);
			}
		}
	}

	void Start() {
		_origin = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate() {
		if (target && origin && throttle != null) {
			int currentThrottleIndex = (int) currentThrottle;

			Vector2 dir = (target.position - origin.position).normalized;

			float originVel = Vector2.Dot(MathC.left (dir), origin.velocity);
			float targetVel = Vector2.Dot(MathC.right(dir), target.velocity);

			origin.AddForce(MathC.left (dir) * throttle[currentThrottleIndex] * (strength - originVel * damping));
			target.AddForce(MathC.right(dir) * throttle[currentThrottleIndex] * (strength - targetVel * damping));

			currentThrottle = (currentThrottle + Time.fixedDeltaTime * 3) % throttle.Length;
		}
	}
}
