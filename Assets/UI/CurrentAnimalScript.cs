﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CurrentAnimalScript : MonoBehaviour {
	public Text text = null;
	public EvolutionManager evolution = null;

	// Update is called once per frame
	void Update () {
		if (evolution && evolution.gameObject.activeInHierarchy && evolution.animalData != null) {
			text.text = (evolution.currentAnimal + 1).ToString() + " / " + evolution.animalData.Count.ToString();
		}
		else {
			text.text = "";
		}
	}
}
