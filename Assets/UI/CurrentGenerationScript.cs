﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CurrentGenerationScript : MonoBehaviour {
	public Text text = null;
	public EvolutionManager evolution = null;

	// Update is called once per frame
	void Update () {
		if (evolution && evolution.gameObject.activeInHierarchy) {
			text.text = evolution.generation.ToString();
		}
		else {
			text.text = "";
		}
	}
}
