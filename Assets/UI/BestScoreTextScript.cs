﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BestScoreTextScript : MonoBehaviour {
	public Text text = null;
	public EvolutionManager evolution = null;

	// Update is called once per frame
	void Update () {
		if (evolution && evolution.gameObject.activeInHierarchy && evolution.generation > 0 && evolution.score.Count > 0) {
			text.text = evolution.score[0].ToString("F2");
		}
		else {
			text.text = "";
		}
	}
}
