﻿using UnityEngine;
using System.Collections;

public class MoveScript : MonoBehaviour {

	protected Vector2 offset = new Vector2();
	protected Transform target = null;

	// Use this for initialization
	void Start () {

	}
	
	void Update() {
		if (Input.GetMouseButtonDown(0)) {
			Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			RaycastHit2D hit = Physics2D.Raycast(mouse, Vector2.right, 0.001f);

			if (hit) {
				target = hit.transform;

				offset = mouse - (Vector2) target.position;
			}
		}
		
		if (Input.GetMouseButtonUp(0)) {
			target = null;
		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (target) {
			Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			target.position = mouse + offset;
		}
	}
}
