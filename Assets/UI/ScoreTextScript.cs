﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreTextScript : MonoBehaviour {
	public Text text = null;
	public Simulation simulation = null;

	// Update is called once per frame
	void Update () {
		if (simulation && simulation.gameObject.activeInHierarchy) {
			text.text = simulation.score.ToString("F2");
		}
		else {
			text.text = "";
		}
	}
}
