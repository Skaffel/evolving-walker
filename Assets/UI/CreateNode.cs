﻿using UnityEngine;
using System.Collections.Generic;

public class CreateNode : MonoBehaviour {
	public GameObject Node = null;

	public List<GameObject> nodes = new List<GameObject>();

	void Update() {
		if (Input.GetMouseButtonDown(0)) {
			Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			GameObject node = Instantiate(Node, mouse, Quaternion.identity) as GameObject;

			node.GetComponent<Rigidbody2D>().isKinematic = true;

			if (nodes.Count == 0) {
				node.GetComponent<MuscleHandler>().setHead();
			}

			nodes.Add(node);
		}
	}
}
