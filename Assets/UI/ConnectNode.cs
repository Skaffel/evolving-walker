﻿using UnityEngine;
using System.Collections;

public class ConnectNode : MonoBehaviour {

	protected Rigidbody2D origin = null;
	protected Rigidbody2D target = null;
	
	// Update is called once per frame
	void Update () {
		// Find target Node
		Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		RaycastHit2D hit = Physics2D.Raycast(mouse, Vector2.right, 0.001f);

		Rigidbody2D current = null;
		if (hit) {
			current = hit.transform.GetComponent<Rigidbody2D>();
		}

		if (Input.GetMouseButtonDown(0) && !origin) {
			origin = current; current = null;
		}

		if (Input.GetMouseButtonUp(0) && origin) {
			target = current; current = null;
		}

		// Connect two nodes
		if (Input.GetMouseButtonUp(0) && origin && target && origin != target) {
			NodeFactory.connectNode(origin, target, new float[]{0}, new float[]{0});
		}
		
		// Failed to connect
		if (Input.GetMouseButtonUp(0)) {
			origin = null;
			target = null;
		}

		// Draw
		if (origin) {
			Color color = Color.black;

			if (current && current != origin) {
				color = Color.green;
			}

			Debug.DrawLine(origin.position, mouse, color);
		}
	}
}
