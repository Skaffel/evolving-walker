﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class MeshOrder : MonoBehaviour {

	public string sortingLayer = "";
	public int sortingOrder = 0;

	protected new Renderer renderer = null;

	void Awake() {
		renderer = transform.GetComponent<Renderer>();
	}

	void Start() {
		renderer.sortingLayerName = sortingLayer;
		renderer.sortingOrder = sortingOrder;
	}

	public void changeSortingOrder(int sortingOrder) {
		renderer.sortingOrder = sortingOrder;

		this.sortingOrder = sortingOrder;
	}
}
