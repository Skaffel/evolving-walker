﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParallaxScrolling : MonoBehaviour {
	public float scrolling = 1;
	public float scale = 1;

	public Vector2 offset = new Vector2();

	public bool hasMinX = false;
	public bool hasMaxX = false;

	public bool hasMinY = false;
	public bool hasMaxY = false;

	public Vector2 min = new Vector2();
	public Vector2 max = new Vector2();

	protected Camera mainCamera = null;
	protected Vector2 cameraSize = new Vector2();

	protected MeshFilter   meshFilter;
	protected MeshRenderer meshRenderer;

	protected Mesh mesh = null;
	protected Vector3[] vertices = null;

	protected bool hasBeenModified = false;

	// Use this for initialization
	void Awake() {
		meshFilter   = GetComponent<MeshFilter>();
		meshRenderer = GetComponent<MeshRenderer>();
	}

	void Start() {
		mainCamera = Camera.main;
		cameraSize = MathC.calculateCameraSize(mainCamera) * 1.2f;

		mesh = createQuad();

		meshFilter.mesh = mesh;

		vertices = mesh.vertices;

		update();
	}

	void OnEnable() {
		meshRenderer.enabled = true;
	}

	void OnDisable() {
		meshRenderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		update();
	}

	protected void update() {
		mainCamera = Camera.main;
		cameraSize = MathC.calculateCameraSize(mainCamera) * 1.2f;
		
		limitVertices();
		updateUV();
	}

	protected void limitVertices() {
		if (hasMinX)
			limitMinX();
		if (hasMaxX)
			limitMaxX();

		if (hasMinY)
			limitMinY();
		if (hasMaxY)
			limitMaxY();

		sanityCheck();

		if (hasBeenModified) {
			mesh.vertices = vertices;

			mesh.RecalculateNormals();

			hasBeenModified = false;
		}
	}

	protected void limitMinX() {
		float x = transform.position.x;

		if (x - cameraSize.x / 2 < min.x) {
			vertices[0].x = min.x - x;
			vertices[1].x = min.x - x;

			hasBeenModified = true;
		}
	}

	protected void limitMaxX() {
		float x = transform.position.x;

		if (x + cameraSize.x / 2 > max.y) {
			vertices[2].x = max.x - x;
			vertices[3].x = max.x - x;

			hasBeenModified = true;
		}
	}

	protected void limitMinY() {
		float y = transform.position.y;

		if (y - cameraSize.y / 2 < min.y) {
			vertices[0].y = min.y - y;
			vertices[3].y = min.y - y;

			hasBeenModified = true;
		}
	}

	protected void limitMaxY() {
		float y = transform.position.y;

		if (y + cameraSize.y / 2 > max.y) {
			vertices[1].y = max.y - y;
			vertices[2].y = max.y - y;

			hasBeenModified = true;
		}
	}

	protected void sanityCheck() {
		if (vertices[0].x > vertices[2].x || vertices[1].x > vertices[3].x) {
			vertices[2].x = vertices[0].x;
			vertices[3].x = vertices[1].x;

			hasBeenModified = true;
		}
		if (vertices[0].y > vertices[1].y || vertices[3].y > vertices[2].y) {
			vertices[1].y = vertices[0].y;
			vertices[2].y = vertices[3].y;

			hasBeenModified = true;
		}
	}

	protected void updateUV() {
		Vector2 cameraPosition = mainCamera.transform.position;
		Vector2 uvOffset = -cameraPosition * scrolling + offset;

		Vector3[] vertices = mesh.vertices;
		Vector2[] uv = MeshMath.mapWorldUVScrolling(vertices, -cameraSize / 2, uvOffset, cameraSize.x * scale);

		mesh.uv = uv;
	}

	public float getCentreOffset() {
		return cameraSize.x / 2;
	}

	public Vector2 getSize() {
		return new Vector2(1, 1) * cameraSize.x * scale / scrolling;
	}

	// Init data
	protected Mesh createQuad() {
		Mesh mesh = new Mesh();

		List<Vector3> vertices  = new List<Vector3>();
		List<int> triangles     = new List<int>();

		vertices.Add(-cameraSize / 2 + new Vector2(0		   , 0	     	  ));
		vertices.Add(-cameraSize / 2 + new Vector2(0           , cameraSize.y ));
		vertices.Add(-cameraSize / 2 + new Vector2(cameraSize.x, cameraSize.y ));
		vertices.Add(-cameraSize / 2 + new Vector2(cameraSize.x, 0            ));
			
		triangles.Add(0);
		triangles.Add(1);
		triangles.Add(2);

		triangles.Add(2);
		triangles.Add(3);
		triangles.Add(0);

		mesh.vertices  = vertices.ToArray();
		mesh.triangles = triangles.ToArray();

		mesh.RecalculateNormals();

		return mesh;
	}

	public void recenter(Vector2 translation) {
		offset -= translation * scrolling;	
	}
}
