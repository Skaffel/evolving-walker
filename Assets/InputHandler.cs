﻿using UnityEngine;
using FullInspector;
using System.Collections;
using System.Collections.Generic;

public class InputHandler : BaseBehavior {
	public GameObject Node = null;

	public int muscleLength = 10;

	public Dictionary<State, GameObject> states = new Dictionary<State, GameObject>();

	protected State state = State.Creating;

	protected AnimalData animalData = null;

	protected bool shouldQuitSimulation = false;

	void Start() {
		changeState();
	}

	void Update() {
		State oldState = state;

		if (Input.GetKeyDown(KeyCode.Return)) {
			store();

			if (animalData != null && states.ContainsKey(State.Evolution) && state != State.Evolution) {
				states[State.Evolution].GetComponent<EvolutionManager>().baseAnimal = animalData;

				state = State.Evolution;
			}
		}
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (state == State.Creating) {
				clean();
				animalData = null;
			}

			store();

			if (animalData != null) {
				GameObject[] nodes = animalData.createAnimal(Node, new Vector2());
				states[State.Creating].GetComponent<CreateNode>().nodes = new List<GameObject>(nodes);
			}

			state = State.Creating;
		}

		if (oldState != state) {
			changeState();
		}
	}


	protected void changeState() {
		foreach (GameObject obj in states.Values) {
			obj.SetActive(false);
		}

		if (states.ContainsKey(state)) {
			states[state].SetActive(true);
		}
	}

	protected void store() {
		if (state != State.Creating)
			return;

		List<GameObject> nodes = states[State.Creating].GetComponent<CreateNode>().nodes;

		if (nodes != null && nodes.Count > 0) {
			animalData = new AnimalData(nodes.ToArray(), muscleLength);
		}

		clean();
	}

	protected void clean() {
		if (state != State.Creating)
			return;

		List<GameObject> nodes = states[State.Creating].GetComponent<CreateNode>().nodes;

		for (int n = 0; nodes != null && n < nodes.Count; n++) {
			Destroy(nodes[n]);
		}

		states[State.Creating].GetComponent<CreateNode>().nodes = new List<GameObject>();
	}

	public enum State {
		Creating,
		Evolution,
	}
}
