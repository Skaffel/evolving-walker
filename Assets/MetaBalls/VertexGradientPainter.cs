﻿using UnityEngine;
using FullInspector;
using System.Collections;

public class VertexGradientPainter : BaseBehavior {
	public Node[] nodes;

	// Use this for initialization
	void Awake() {
		setColor();
	}

	void Update() {
		setColor();
	}

	public void setColor() {
		MeshFilter meshFilter = GetComponent<MeshFilter>();

		if (meshFilter) {
	     	Mesh mesh = meshFilter.mesh;

	  		updateColor(mesh); 	
		}
	}

	private void updateColor(Mesh mesh) {
		Vector3[] vertices = mesh.vertices;
		
		Color[] colors = new Color[mesh.vertices.Length];

		for (int n = 0; n < vertices.Length; n++) {
			float maxWeight = 0;
			float totalWeight = 0;

			for (int i = 0; i < nodes.Length; i++) {
				float weight = ((Vector2) (vertices[n]) - nodes[i].position).magnitude;

				if (weight > maxWeight) {
					maxWeight = weight;
				}
			}

			for (int i = 0; i < nodes.Length; i++) {
				float weight = maxWeight - ((Vector2) (vertices[n]) - nodes[i].position).magnitude;

				colors[n] += weight * nodes[i].color;
				totalWeight += weight;
			}

			if (totalWeight == 0) {
				totalWeight = 1;
			}

			colors[n] /= totalWeight;
		}

		mesh.colors = colors;
	}

	[SerializeField]
	public struct Node {
		public Node(Color color = new Color(), Vector2 position = new Vector2()) {
			this.color = color;
			this.position = position;
		}

		public Vector2 position;
		public Color color;
	}
}
