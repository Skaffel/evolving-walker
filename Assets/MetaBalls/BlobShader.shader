﻿Shader "Custom/BlobShader" {
Properties {
	_MainTex ("Particle Texture", 2D) = "white" {}
}

Category {
	Tags 
	{
		"Queue"="Transparent" 
		"IgnoreProjector"="True"
		"RenderType"="Transparent"
	}
	
	Blend SrcAlpha OneMinusSrcAlpha, One One
	BlendOp Add, Add
	ColorMask ARGB
	Cull Off
	Lighting Off
	ZWrite Off

	SubShader {
		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
			};
			
			sampler2D _MainTex;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color;
				
				return OUT;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;

				return c;
			}
		ENDCG
		}
	}	
}
}
