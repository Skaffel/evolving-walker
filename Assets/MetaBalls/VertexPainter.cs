﻿using UnityEngine;
using System.Collections;

public class VertexPainter : MonoBehaviour {
	public Color color = Color.white;

	// Use this for initialization
	void Awake() {
		setColor(color);
	}

	public void setColor(Color color) {
		this.color = color;

		MeshFilter meshFilter = GetComponent<MeshFilter>();

		if (meshFilter) {
			updateColor(meshFilter.mesh); 	
		}
	}

	private void updateColor(Mesh mesh) {
		Color[] colors = new Color[mesh.vertices.Length];

		for (int n = 0; n < colors.Length; n++) {
			colors[n] = color;
		}

		mesh.colors = colors;
	}
}
