Shader "Custom/OutlineShader" {
Properties {
	_MainTex   ("Main Texture", 2D) = "white" {}
	_NormalTex ("Normal Texture", 2D) = "white" {}

	_OutlineColor("Outline Color", Color)               =  (0, 0, 0, 1)
	_OutlineThreshold("Outline Threshold", Range(0, 1)) = 0
}

Category {
	Tags 
	{
		"Queue"="Transparent" 
		"IgnoreProjector"="True"
		"RenderType"="Transparent"
	}
	
	
	Blend Off
	//BlendOp Add, Add
	ColorMask ARGB
	Cull Off
	Lighting Off
	ZWrite Off

	SubShader {
		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
			};
			
			sampler2D _MainTex;
			sampler2D _NormalTex;

			fixed4 _OutlineColor;
			float _OutlineThreshold;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color;
				
				return OUT;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c  = tex2D(_MainTex, IN.texcoord) * IN.color;
				fixed  r  = tex2D(_NormalTex, IN.texcoord).r;

				if (r > _OutlineThreshold) {
					//c.rgb = c.rgb * (1 - r) + _OutlineColor.rgb * r;
					//c.a = c.a * c.a;
				}

				return c;
			}
		ENDCG
		}
	}	
}
}
