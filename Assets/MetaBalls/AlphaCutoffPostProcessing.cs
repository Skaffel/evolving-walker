﻿using UnityEngine;
using System.Collections;

public class AlphaCutoffPostProcessing : MonoBehaviour {
	public Material cutoffMaterial;
	public RenderTexture cutoffTexture;

	public Material normalMapMaterial;
	public RenderTexture normalMap;

	public Material outlineMaterial;

	void OnRenderImage (RenderTexture source, RenderTexture destination) {
		Graphics.SetRenderTarget(cutoffTexture);
		GL.Clear(true, true, new Color(0, 0, 0, 0));
		Graphics.SetRenderTarget(null);

 		Graphics.Blit(source, cutoffTexture, cutoffMaterial);

 		//
 		Graphics.SetRenderTarget(normalMap);
		GL.Clear(true, true, new Color(0, 0, 0, 0));
		Graphics.SetRenderTarget(null);

		Graphics.Blit(cutoffTexture, normalMap, normalMapMaterial);

		//
		/*Graphics.SetRenderTarget(destination);
		GL.Clear(true, true, new Color(0, 0, 0, 0));
		Graphics.SetRenderTarget(null);

 		Graphics.Blit(cutoffTexture, destination, outlineMaterial);*/
	}
}
