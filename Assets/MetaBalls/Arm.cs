﻿using UnityEngine;
using System.Collections;

public class Arm : MonoBehaviour {
	//public MeshFilter meshFilter = null;

	public int numberOfPoints = 5;

	[Range(0.0f, 1.0f)]
	public float curvature = 1f;

	public LineRenderer line = null;

	public Rigidbody2D origin = null;
	public Rigidbody2D target = null;

	public SpringJoint2D joint = null;

	public MuscleHandler handler;

	public int index;

	// Update is called once per frame
	void Start() {
		line.SetWidth(1f, 1f);
	}

	void LateUpdate() {
		Vector2 D1 = handler.      modifiedDirections[index];
		Vector2 D2 = handler.targetModifiedDirections[index];

		Vector2 P1 = origin.position;
		Vector2 P4 = target.position;

		float magnitude = curvature * (P4 - P1).magnitude / 2;

		Vector2 P2 = P1 + magnitude * D1;
		Vector2 P3 = P4 + magnitude * D2;

		line.SetVertexCount(numberOfPoints);

		for (int n = 0; n < numberOfPoints; n++) {
			line.SetPosition(n, getPoint(P1, P2, P3, P4, (float) (n) / (numberOfPoints - 1f)));
		}

		Color originColor = origin.GetComponent<VertexPainter>().color;
		Color targetColor = target.GetComponent<VertexPainter>().color;

		line.SetColors(originColor, targetColor);

		float width = 1.5f + (joint.distance - (target.position - origin.position).magnitude) / 10f;

		width = Mathf.Clamp(width, 1f, 1.5f);

		line.SetWidth(width, width);
	}

	private static Vector2 getPoint(Vector2 P1, Vector2 P2, Vector2 P3, Vector2 P4, float t) {
		return Mathf.Pow(1 - t, 3) * P1 + 3 * Mathf.Pow(1 - t, 2) * t  * P2 + 3 * (1 - t) * t * t * P3 + t * t * t * P4;
	}
}
