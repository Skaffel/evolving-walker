﻿Shader "Custom/CalculateNormalShader"
{
	Properties
	{
		_MainTex ("Sprite Texture", 2D) = "white" {}
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
		}

		ColorMask R
		Cull Off
		Lighting Off
		ZWrite Off
		Blend Off

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
			};
			
			sampler2D _MainTex;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color;

				return OUT;
			}

			fixed frag(v2f IN) : SV_Target
			{	
				fixed o    = 0;
				fixed4 c   = tex2D(_MainTex, IN.texcoord);
				fixed4 cx1 = tex2D(_MainTex, IN.texcoord - float2(1.0 / 2048.0, 0));
				fixed4 cx2 = tex2D(_MainTex, IN.texcoord + float2(1.0 / 2048.0, 0));

				fixed4 cy1 = tex2D(_MainTex, IN.texcoord - float2(0, 1.0 / 1024.0));
				fixed4 cy2 = tex2D(_MainTex, IN.texcoord + float2(0, 1.0 / 1024.0));

				fixed2 n;

				n.x = abs(cx1.a - cx2.a);
				n.y = abs(cy1.a - cy2.a);
				
				o = sqrt(n.x * n.x + n.y * n.y) / sqrt(2);

				return o;
			}
		ENDCG
		}
	}
}
