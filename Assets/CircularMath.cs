﻿using UnityEngine;
using System.Collections.Generic;

public static class CircularMath {
	public static float subtraction(float A, float B) {
		float R = B - A;

		if (R > Mathf.PI) {
			R = R - 2 * Mathf.PI;
		}
		else if (R < -Mathf.PI) {
			R = R + 2 * Mathf.PI;
		}

		return R;
	}

	public static float addition(float A, float B) {
		float R = B + A;

		if (R > Mathf.PI) {
			R = R - 2 * Mathf.PI;
		}
		else if (R < -Mathf.PI) {
			R = R + 2 * Mathf.PI;
		}

		return R;
	}

	public static float mean(float[] angles) {
		if (angles.Length == 0)
			return 0;

		float sin = 0;
		float cos = 0;

		for (int n = 0; n < angles.Length; n++) {
			sin += Mathf.Sin(angles[n]);
			cos += Mathf.Cos(angles[n]);
		}

		return Mathf.Atan2(sin / angles.Length, cos / angles.Length);
	}
}
