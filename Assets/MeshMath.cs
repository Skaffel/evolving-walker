using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MeshMath {
	public static void mapWorldUV(List<Vector2> uv, List<Vector3> vertices, Vector2 origin, float textureScale) {
		for (int n = 0; n < vertices.Count; n++) {
			float x = (vertices[n].x + origin.x % textureScale) / textureScale;
			float y = (vertices[n].y + origin.y % textureScale) / textureScale;

			uv.Add(new Vector2(x, y));
		}
	}

	public static Vector2[] mapWorldUVScrolling(Vector3[] vertices, Vector2 origin, Vector2 offset, float textureScale) {
		Vector2[] uv = new Vector2[vertices.Length];

		for (int n = 0; n < vertices.Length; n++) {
			float x = (vertices[n].x - origin.x - offset.x % textureScale) / textureScale;
			float y = (vertices[n].y - origin.y - offset.y % textureScale) / textureScale;

			uv[n] = new Vector2(x, y);
		}

		return uv;
	}

	public static void setGradientTint(Mesh mesh, Color min, Color max) {
		Vector3[] vertices = mesh.vertices;
		Color[] colors = new Color[vertices.Length];

		if (vertices.Length > 2) {
	        colors[0] = min;
	        colors[1] = max;
	    }

		for (int n = 2; n < vertices.Length; n += 2) {
            colors[n    ] = max;
            colors[n + 1] = min;
        }

        mesh.colors = colors;
	}
}